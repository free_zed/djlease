DjLease
========

A lease management tool built with [Django][django].

E-commerce solutions are focused on selling stuff that never come back in your stock (at least I hope for your business).

Here the point is focused on references (products), your stock & the care you give to your client over time :

* where are my references (in stock or away) ?
* what my stock looks like at given date ?
* did I send the <whatever step> message to my client ?
* which orders are planned next day/week/month ?
* …

Ressources
----------

* [Issues list][issues]
* [Project activity][activity]

[activity]: https://gitlab.com/free_zed/djlease/activity "Project activity via Gitlab"
[django]: https://www.djangoproject.com/ "The web framework for perfectionists with deadlines"
[issues]:   https://gitlab.com/free_zed/djlease/issues "Issues list via Gitlab"
